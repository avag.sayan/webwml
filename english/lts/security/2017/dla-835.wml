<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Dawid Golunski from legalhackers.com discovered that cakephp, an
application development framework for PHP, contains a vulnerability
that allows attackers to spoof the source IP address. It would allow
them to bypass access control lists, or the injection of malicious
data which, if treated as sanitized by an unaware CakePHP-based
application, can lead to other vulnerabilities such as SQL injection,
XSS or command injection.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.15-1+deb7u2.</p>

<p>We recommend that you upgrade your cakephp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-835.data"
# $Id: $
