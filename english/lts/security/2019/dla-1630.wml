<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were corrected in the libav
multimedia library which may lead to a denial-of-service, information
disclosure or the execution of arbitrary code if a malformed file is
processed.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9993">CVE-2017-9993</a>

    <p>Libav does not properly restrict HTTP Live Streaming filename
    extensions and demuxer names, which allows attackers to read
    arbitrary files via crafted playlist data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9994">CVE-2017-9994</a>

    <p>libavcodec/webp.c in Libav does not ensure that pix_fmt is set,
    which allows remote attackers to cause a denial of service
    (heap-based buffer overflow and application crash) or possibly have
    unspecified other impact via a crafted file, related to the
    vp8_decode_mb_row_no_filter and pred8x8_128_dc_8_c functions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14055">CVE-2017-14055</a>

    <p>Denial-of-service in mv_read_header() due to lack of an EOF (End of
    File) check might cause huge CPU and memory consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14056">CVE-2017-14056</a>

    <p>Denial-of-service in rl2_read_header() due to lack of an EOF
    (End of File) check might cause huge CPU and memory consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14057">CVE-2017-14057</a>

    <p>Denial-of-service in asf_read_marker() due to lack of an EOF
   (End of File) check might cause huge CPU and memory consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14170">CVE-2017-14170</a>

    <p>Denial-of-service in mxf_read_index_entry_array() due to lack of an
    EOF (End of File) check might cause huge CPU consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14171">CVE-2017-14171</a>

    <p>Denial-of-service in nsv_parse_NSVf_header() due to lack of an EOF
   (End of File) check might cause huge CPU consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14767">CVE-2017-14767</a>

    <p>The sdp_parse_fmtp_config_h264 function in
    libavformat/rtpdec_h264.c mishandles empty sprop-parameter-sets
    values, which allows remote attackers to cause a denial of service
    (heap buffer overflow) or possibly have unspecified other impact via
    a crafted sdp file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15672">CVE-2017-15672</a>

    <p>The read_header function in libavcodec/ffv1dec.c allows remote
    attackers to have unspecified impact via a crafted MP4 file, which
    triggers an out-of-bounds read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17130">CVE-2017-17130</a>

    <p>The ff_free_picture_tables function in libavcodec/mpegpicture.c
    allows remote attackers to cause a denial of service
    (heap-based buffer overflow and application crash) or possibly have
    unspecified other impact via a crafted file, related to
    vc1_decode_i_blocks_adv.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6621">CVE-2018-6621</a>

    <p>The decode_frame function in libavcodec/utvideodec.c in Libav allows
    remote attackers to cause a denial of service (out of array read)
    via a crafted AVI file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7557">CVE-2018-7557</a>

    <p>The decode_init function in libavcodec/utvideodec.c in
    Libav allows remote attackers to cause a denial of service
    (Out of array read) via an AVI file with crafted dimensions within
    chroma subsampling data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14394">CVE-2018-14394</a>

    <p>libavformat/movenc.c in Libav allows attackers to cause a
    denial of service (application crash caused by a divide-by-zero
    error) with a user crafted Waveform audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1999010">CVE-2018-1999010</a>

    <p>Libav contains multiple out of array access vulnerabilities in the
    mms protocol that can result in attackers accessing out of bound
    data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
6:11.12-1~deb8u4.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1630.data"
# $Id: $
