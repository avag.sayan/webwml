<define-tag description>LTS regression update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were was a regression introduced in DLA-2145-1
due to the incorrect application of the upstream patch for CVE-2020-10108 &amp;
CVE-2020-10109 regarding a number of HTTP request splitting vulnerabilities in
Twisted, an Python event-based framework for building various types of internet
applications.</p>

<p>Thanks to Etienne Allovon for the detailed report.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10108">CVE-2020-10108</a>

    <p>In Twisted Web through 19.10.0, there was an HTTP request splitting
    vulnerability. When presented with two content-length headers, it ignored
    the first header. When the second content-length value was set to zero, the
    request body was interpreted as a pipelined request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10109">CVE-2020-10109</a>

    <p>In Twisted Web through 19.10.0, there was an HTTP request splitting
    vulnerability. When presented with a content-length and a chunked encoding
    header, the content-length took precedence and the remainder of the request
    body was interpreted as a pipelined request.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
14.0.2-3+deb8u2.</p>

<p>We recommend that you upgrade your twisted packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2145-2.data"
# $Id: $
