#!/usr/bin/python3

import argparse
import logging
import os.path
import re

import requests

# regex used in data/DLA/list and data/DSA/list files to identify
# advisories
RE_ADV = r'\[\d+\s+\w+\s+(?P<year>\d+)\]\s+D[SL]A-(?P<number>\d+)(?:-(?P<errata>\d+))?'  # noqa: E501
RE_SKIP = r'^\s+'

# the URL for the lists if not provided locally, joker replaced with
# DSA or DLA depending on --mode
BASE_URL = 'https://salsa.debian.org/security-tracker-team/security-tracker/raw/master/data/%s/list'  # noqa: E501


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized befure this, using
    `basicConfig`.
    """
    def __init__(self, *args, **kwargs):
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs['choices'] = logging._nameToLevel.keys()
        if 'const' in kwargs:
            kwargs['nargs'] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger('').setLevel(self.const)
        else:
            logging.getLogger('').setLevel(values)


def main():
    logging.basicConfig(format='%(levelname)s: %(message)s', level='WARNING')
    parser = argparse.ArgumentParser(description='Find missing advisories, and optionally author information if python3-git is installed')
    parser.add_argument('--tracker', help='security-tracker path (default: fetch)')
    parser.add_argument('--directory',
                        help='website path (default: english/security or english/lts/security, depending on mode)')  # noqa: E501
    parser.add_argument('--verbose', action=LoggingAction, const='INFO',
                        help='show more progress information')
    parser.add_argument('--debug', action=LoggingAction, const='DEBUG',
                        help='show debug information')
    parser.add_argument('--mode', default='DSA', choices=('DSA', 'DLA'),
                        help='which sort of advisory to check (default: %(default)s)')  # noqa: E501

    args = parser.parse_args()
    if not args.directory:
        webwml_dir = os.path.dirname(__file__) + '/../../'
        if args.mode == 'DSA':
            args.directory = webwml_dir + 'english/security'
        elif args.mode == 'DLA':
            args.directory = webwml_dir + 'english/lts/security'

    if not args.tracker:
        url = BASE_URL % args.mode
        logging.info('fetching URL %s', url)
        response = requests.get(url)
        response.raise_for_status()
        for adv in parse_advisories(response.iter_lines(decode_unicode=True)):
            check_advisory(args.mode, args.directory, **adv)
    else:
        try:
            import git

            repo = git.Repo(args.tracker)
            for commit, lines in repo.blame('HEAD', 'data/%s/list' % args.mode):
                for adv in parse_advisories(lines):
                    check_advisory(args.mode, args.directory, **adv, author=commit.author)
        except ImportError:
            logging.debug('git module not found, will not report author information')
            with open(args.tracker + 'data/%s/list' % args.mode) as text:
                for adv in parse_advisories(text):
                    check_advisory(args.mode, args.directory, **adv)


def parse_advisories(stream):
    for line in stream:
        m = re.match(RE_ADV, line)
        if m:
            yield m.groupdict()
        elif re.match(RE_SKIP, line):
            logging.debug('skipping line: "%s"', line)
        else:
            logging.warning('malformed line: "%s"', line)


def check_advisory(mode, directory, year, number, errata, author=None):
    if errata is None:
        errata = '1'

    if int(errata) >= 2 and mode == 'DSA':
        logging.info('skipping DSA regression update %s-%s-%s (%s)',
                     mode, number, errata, year)
        return

    logging.info('checking %s-%s-%s (%s)', mode, number, errata, year)
    path = "%s/%s/%s-%s-%s" % (directory, year, mode.lower(), number, errata)
    found = False
    if os.path.exists(path + '.data') and os.path.exists(path + '.wml'):
        logging.debug('both data and wml files found')
        found = True
    elif errata == '1':
        path = "%s/%s/%s-%s" % (directory, year, mode.lower(), number)
        if os.path.exists(path + '.data') and os.path.exists(path + '.wml'):
            logging.debug('both data and wml files found, without -1')
            found = True
    if not found:
        author_info = ""
        if author:
            author_info = "(reserved by %s)" % (author)
        logging.error('.data or .wml file missing for %s %s-%s %s',
                      mode, number, errata, author_info)


if __name__ == '__main__':
    main()
