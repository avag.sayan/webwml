#use wml::debian::template title="Πληροφορίες για την ασφάλεια" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="94fd1c6cb95cf528040d57befa96eac2dd4d5d0f" maintainer="galaxico"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<p>Το Debian αντιμετωπίζει πολύ σοβαρά το ζήτημα της ασφάλειας. Αντιμετωπίζουμε 
όλα τα προβλήματα ασφαλείας που έρχονται στην προσοχή μας και διασφαλίζουμε ότι 
διορθώνονται μέσα σε ένα εύλογο χρονικό διάστημα. Αρκετές ειδοποιήσεις 
ασφαλείας αντιμετωπίζονται σε συντονισμό και με άλλους προμηθευτές ελεύθερου 
λογισμικού και δημοσιεύονται τη ίδια μέρα που μια ευαλωτότητα δημοσιοποιείται· 
επιπλέον έχουμε μια ομάδα για <a href="audit/">Ελέγχου Ασφάλειας</a> που 
επιθεωρεί τις αρχειοθήκες των πακέτων για καινούρια ή μη διορθωμένα σφάλματα 
ασφαλείας.</p>

# "reasonable timeframe" might be too vague, but we don't have 
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.

<p>Η εμπειρία έχει δείξει ότι τακτική της <q>ασφάλειας μέσω της 
αδιαφάνειας</q> δεν δουλεύει. Η δημόσια έκθεση επιτρέπει πολύ πιο ταχείες και 
καλλίτερες λύσεις στα προβλήματα ασφαλείας. Με αυτό το πνεύμα, η παρούσα σελίδα 
παρουσιάζει την κατάσταση του Debian σε σχέση με διάφορες γνωστές "τρύπες" 
ασφαλείας, που θα μπορούσαν δυνητικά να επηρεάσουν το Debian.</p>  

<p>Το Debian συμμετέχει επίσης στις προσπάθειες εφαρμογής προτύπων στην 
ασφάλεια:
οι <a href="#DSAS">Ανακοινώσεις Ασφαλείας του Debian</a> είναι
<a href="cve-compatibility">συμμορφωμένες με το CVE</a> 
(δείτε τις <a href="crossreferences">παραπομπές</a>) και αντιπροσωπεύεται 
στο Συμβούλιο του Σχεδίου 
<a href="https://oval.cisecurity.org/">Open Vulnerability Assessment 
Language</a>.
</p>


<toc-add-entry name="keeping-secure">Κρατήστε το Debian σύστημά σας 
ασφαλές</toc-add-entry>

<p>Για να λαμβάνετε τις πιο πρόσφατες ανακοινώσεις ασφαλείας του Debian 
εγγραφείτε στη λίστα αλληλογραφίας <a 
href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>.</p>

<p>Μπορείτε να χρησιμοποιήσετε το <a 
href="https://packages.debian.org/stable/admin/apt">apt</a>
για να παίρνετε τις πιο πρόσφατες αναβαθμίσεις ασφαλείας. Το μόνο που 
χρειάζεται είναι μια γραμμή όπως η ακόλουθη
</p>
<div class="centerblock">
<p>
<code>deb&nbsp;http://security.debian.org/debian-security&nbsp;<current_release_name>/updates&nbsp;main&nbsp;contrib&nbsp;non-free</code>
</p>
</div>
<p>
στο αρχείο <CODE>/etc/apt/sources.list</CODE>. Στη συνέχεια εκτελέστε τις 
εντολές
<kbd>apt-get update &amp;&amp; apt-get upgrade</kbd> για να μεταφορτώσετε και 
να εφαρμόσετε τις εκκρεμούσες αναβαθμίσεις. Η αρχειοθήκη ασφαλείας είναι 
υπογεγγραμμένη με τα συνήθη 
<a href="https://ftp-master.debian.org/keys.html">ψηφιακά κλειδιά</a> των 
αρχειοθηκών του Debian.
</p>

<p>Για περισσότερες πληροφορίες σχετικά με τα ζητήματα ασφαλείας στο Debian, 
παρακαλούμε δείτε τη σελίδα 
the <a href="faq">Συχνές ερωτήσεις της Ομάδας Ασφαλείας</a> 
και το εγχειρίδιο με τίτλο
<a href="../doc/user-manuals#securing">Securing Debian</a>.</p>


<a class="rss_logo" href="dsa">RSS</a>
<toc-add-entry name="DSAS">Πρόσφατες προειδοποιήσεις ασφαλείας</toc-add-entry>

<p>Οι παρακάτω ιστοσελίδες περιέχουν ένα συμπυκνωμένο αρχείο των 
προειδοποιήσεων ασφαλείας που δημοσιεύονται στη λίστα αλληλογραφίας
the <a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}
<p>Οι πιο πρόσφατες προειδοποιήσεις ασφαλείας του Debian είναι επίσης 
διαθέσιμες σε
<a href="dsa">μορφή RDF</a>. Προσφέρουμε επίσης και ένα 
<a href="dsa-long">δεύτερο αρχείο</a> που περιλαμβάνει την πρώτη παράγραφο της 
αντίστοιχης προειδοποίησης ώστε να μπορείτε να δείτε τι αφορά αυτή η 
προειδοποίηση.</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>Είναι επίσης διαθέσιμες και οι παλιότερες προειδοποιήσεις ασφαλείας:
<:= get_past_sec_list(); :>

<p>Οι διανομές του Debian δεν είναι ευάλωτες σε όλα τα προβλήματα ασφαλείας. Ο
<a href="https://security-tracker.debian.org/">Ιχνηλάτης Ασφαλείας του 
Debian</a> συλλέγει όλες τις πληροφορίες σχετικά με την κατάσταση ευαλωτότητας 
των πακέτων Debian και μπορείτε να κάνετε αναζήτηση με βάση είτε το όνομα CVE 
είτε το όνομα του πακέτου.</p>


<toc-add-entry name="contact">Πληροφορίες επικοινωνίας</toc-add-entry>

<p>Παρακαλούμε διαβάστε τη σελίδα με τις <a href="faq">Συχνές ερωτήσεις 
της Ομάδας Ασφαλείας</a> πριν επικοινωνήσετε μαζί μας, το ερώτημά σας 
μπορεί να έχει απαντηθεί εκεί ήδη!</p>

<p>Οι <a 
href="faq#contact">πληροφορίες επικοινωνίας βρίσκονται επίσης στη σελίδα 
των Συχνών ερωτήσεων</a>.</p>
