# Emmanuel Galatoulas <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2019-07-15 15:34+0200\n"
"Last-Translator: galaxico <galas@tee.gr>\n"
"Language-Team: Greek <debian-www@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/blends/blend.defs:15
msgid "Metapackages"
msgstr "Μεταπακέτα"

#: ../../english/blends/blend.defs:18
msgid "Downloads"
msgstr "Μεταφορτώσεις"

#: ../../english/blends/blend.defs:21
msgid "Derivatives"
msgstr "Παράγωγες διανομές"

#: ../../english/blends/released.data:15
msgid ""
"The goal of Debian Astro is to develop a Debian based operating system that "
"fits the requirements of both professional and hobby astronomers. It "
"integrates a large number of software packages covering telescope control, "
"data reduction, presentation and other fields."
msgstr ""
"Ο στόχος της Debian Astro είναι να αναπτύξει ένα ΛΣ βασισμένο στο Debian που "
"να ταιριάζει στις απαιτήσεις τόσο των επαγγελματιών όσο και των ερασιτεχνών "
"αστρονόμων. Ενσωματώνει έναν μεγάλο αριθμό πακέτων λογισμικού που καλύπτουν "
"τον έλεγχο τηλεσκοπίου, αναγωγή των δεδομένων (data reduction), παρουσίαση "
"και άλλα πεδία."

#: ../../english/blends/released.data:23
msgid ""
"The goal of DebiChem is to make Debian a good platform for chemists in their "
"day-to-day work."
msgstr ""
"Ο σκοπός της DebiChem είναι να κάνει το Debian μια καλή πλατφόρμα για τους "
"χημικούς στην καθημερινή τους δουλειά."

#: ../../english/blends/released.data:31
msgid ""
"The goal of Debian Games is to provide games in Debian from arcade and "
"adventure to simulation and strategy."
msgstr ""
"Ο στόχος της Debian Games είναι να προσφέρει παιχνίδια στο Debian από παλιά "
"ηλεκτρονικά και παιχνίδια περιπέτειας μέχρι παιχνίδια προσομοίωσης και "
"στρατηγικής."

#: ../../english/blends/released.data:39
msgid ""
"The goal of Debian Edu is to provide a Debian OS system suitable for "
"educational use and in schools."
msgstr ""
"Ο σκοπός της Debian Edu είναι να προσφέρει ένα ΛΣ Debian κατάλληλο για "
"εκπαιδευτική χρήση και χρήση στα σχολεία."

#: ../../english/blends/released.data:47
msgid ""
"The goal of Debian GIS is to develop Debian into the best distribution for "
"Geographical Information System applications and users."
msgstr ""
"Ο σκοπός της Debian GIS είναι να αναπτύξει το Debian στην καλλίτερη δυνατή "
"διανομή για εφαρμογές και χρήστες Γεωπληροφοριακών Συστημάτων."

#: ../../english/blends/released.data:57
msgid ""
"The goal of Debian Junior is to make Debian an OS that children will enjoy "
"using."
msgstr ""
"Ο σκοπός της Debian Junior είναι να κάνει το Debian ένα ΛΣ που τα παιδιά θα "
"ευχαριστιούνται να χρησιμοποιούν."

#: ../../english/blends/released.data:65
msgid ""
"The goal of Debian Med is a complete free and open system for all tasks in "
"medical care and research. To achieve this goal Debian Med integrates "
"related free and open source software for medical imaging, bioinformatics, "
"clinic IT infrastructure, and others within the Debian OS."
msgstr ""
"Ο σκοπός της Debian Med είναι ένα πλήρες ελεύθερο και ανοιχτό σύστημα για "
"όλα τα καθήκοντα στον τομέα της ιατρικής φροντίδας και έρευνας. Για να "
"επιτύχει αυτό τον σκοπό, το Debian Med ενσωματώνει σχετικό ελεύθερο και "
"ανοιχτό λογισμικό για ιατρική απεικόνιση, βιοπληροφορική, υποδομή "
"Τεχνολογιών Πληροφορικής για κλινική και άλλα συστατικά του ΛΣ Debian."

#: ../../english/blends/released.data:73
msgid ""
"The goal of Debian Multimedia is to make Debian a good platform for audio "
"and multimedia work."
msgstr ""
"Ο σκοπός της Debian Multimedia είναι να κάνει το Debian μια καλή πλατφόρμα "
"για εργασία που έχει να κάνει με τον ήχο και τα πολυμέσα."

#: ../../english/blends/released.data:81
msgid ""
"The goal of Debian Science is to provide a better experience when using "
"Debian to researchers and scientists."
msgstr ""
"Ο σκοπός του Debian Science είναι να προσφέρει μια καλλίτερη εμπειρία στη "
"χρήση του Debian από ερευνητές και επιστήμονες."

#: ../../english/blends/unreleased.data:15
msgid ""
"The goal of Debian Accessibility is to develop Debian into an operating "
"system that is particularly well suited for the requirements of people with "
"disabilities."
msgstr ""
"Ο σκοπός της Debian Accessibility είναι η ανάπτυξη του Debian σε ένα ΛΣ που "
"να είναι ιδιαίτερα καλά προσαρμοσμένο στις απαιτήσεις των ατόμων με ειδικές "
"ανάγκες."

#: ../../english/blends/unreleased.data:23
msgid ""
"The goal of Debian Design is to provide applications for designers. This "
"includes graphic design, web design and multimedia design."
msgstr ""
"Ο σκοπός της Debian Design είναι να προσφέρει εφαρμογές για σχεδιαστές. Αυτό "
"περιλαμβάνει γραφική σχεδίαση, σχεδίαση ιστότοπων και σχεδίαση πολυμέσων."

#: ../../english/blends/unreleased.data:30
msgid ""
"The goal of Debian EzGo is to provide culture-based open and free technology "
"with native language support and appropriate user friendly, lightweight and "
"fast desktop environment for low powerful/cost hardwares to empower human "
"capacity building and technology development  in many areas and regions, "
"like Africa, Afghanistan, Indonesia, Vietnam  using Debian."
msgstr ""
"Ο σκοπός της Debian EzGo είναι να προσφέρει ανοιχτή και ελεύθερη τεχνολογία "
"βασισμένη στον πολιτισμό με υποστήριξη για τη μητρική γλώσσα και κατάλληλο  "
"φιλικό προς τον χρήστη, ελαφρύ και γρήγορο περιβάλλον επιφάνειας εργασίας  "
"για χαμηλού κόστους ισχυρό υλικό που να ενδυναμώνει την ανάπτυξη των "
"ικανοτήτων των ανθρώπων και την ανάπτυξη της τεχνολογίας σε πολλές περιοχές "
"και περιφέρειες όπως η Αφρική, το Αφγανιστάν, την Ινδονησία, το Βιετνάμ με "
"τη χρήση του Debian."

#: ../../english/blends/unreleased.data:38
msgid ""
"The goal of FreedomBox is to develop, design and promote personal servers "
"running free software for private, personal communications. Applications "
"include blogs, wikis, websites, social networks, email, web proxy and a Tor "
"relay on a device that can replace a wireless router so that data stays with "
"the users."
msgstr ""
"Ο σκοπός της FreedomBox είναι η ανάπτυξη, σχεδιασμός και προώθηση προσωπικών "
"εξυπηρετητών που να τρέχουν ελεύθερο λογισμικό για ιδιωτική, προσωπική "
"επικοινωνία. Οι εφαρμογές περιλαμβάνουν ιστολόγια, ιστότοπους, κοινωνικά "
"δίκτυα, ηλεκτρονικό ταχυδρομείο,  web proxy και έναν αναμεταδότη (relay) Tor "
"σε μια μοναδική συσκευή η οποία μπορεί να αντικαταστήσει έναν ασύρματο "
"δρομολογητή ώστε τα δεδομένα να παραμένουν με τους χρήστες."

#: ../../english/blends/unreleased.data:46
msgid ""
"The goal of Debian Hamradio is to support the needs of radio amateurs in "
"Debian by providing logging, data mode and packet mode applications and more."
msgstr ""
"Ο σκοπός της Debian Hamradio είναι η υποστήριξη των αναγκών των "
"ραδιοερασιτεχνών στοDebian προσφέροντας εφαρμογές καταγραφής, κατάστασης "
"δεδομένων (data mode), κατάστασης πακέτων (packet mode) και άλλα."

#: ../../english/blends/unreleased.data:55
msgid ""
"The goal of DebianParl is to provide applications to support the needs of "
"parliamentarians, politicians and their staffers all around the world."
msgstr ""
"Ο σκοπός της DebianParl είναι να προσφέρει υποστήριξη για τις ανάγκες "
"βουλευτών, πολιτικών και του προσωπικού τους σ' ολόκληρο τον κόσμο."
