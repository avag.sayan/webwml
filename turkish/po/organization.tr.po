# Turkish messages for Debian Web pages (debian-webwml).
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as Debian Web pages.
#
# Murat Demirten <murat@debian.org>, 2002, 2003, 2004.
# Recai Oktaş <roktas@omu.edu.tr>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-11-10 15:06+0100\n"
"PO-Revision-Date: 2006-01-18 05:44+0200\n"
"Last-Translator: Recai Oktaş <roktas@omu.edu.tr>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=1; plural=0;\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "şu an"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "üye"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:34
#, fuzzy
msgid "Stable Release Manager"
msgstr "Sürüm Yöneticisi"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
#, fuzzy
msgid "chair"
msgstr "başkan"

#: ../../english/intro/organization.data:41
#, fuzzy
msgid "assistant"
msgstr "Sürüm Yardımcıları"

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr "sekreter"

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:47
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:75
msgid "Officers"
msgstr "Ofis Üyeleri"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:99
msgid "Distribution"
msgstr "Dağıtım"

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:235
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:238
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:242
#, fuzzy
msgid "Publicity team"
msgstr "Tanıtım"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr "Destek ve Altyapı"

#: ../../english/intro/organization.data:78
msgid "Leader"
msgstr "Lider"

#: ../../english/intro/organization.data:80
msgid "Technical Committee"
msgstr "Teknik Komite"

#: ../../english/intro/organization.data:94
msgid "Secretary"
msgstr "Sekreter"

#: ../../english/intro/organization.data:102
msgid "Development Projects"
msgstr "Geliştirilen Projeler"

#: ../../english/intro/organization.data:103
msgid "FTP Archives"
msgstr "FTP Arşivleri"

#: ../../english/intro/organization.data:105
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:111
#, fuzzy
msgid "FTP Assistants"
msgstr "Sürüm Yardımcıları"

#: ../../english/intro/organization.data:116
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:120
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:122
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:126
msgid "Release Management"
msgstr "Sürüm Yönetimi"

#: ../../english/intro/organization.data:128
#, fuzzy
msgid "Release Team"
msgstr "Sürüm Notları"

#: ../../english/intro/organization.data:141
msgid "Quality Assurance"
msgstr "Kalite Güvencesi"

#: ../../english/intro/organization.data:142
msgid "Installation System Team"
msgstr "Kurulum Sistemi Ekibi"

#: ../../english/intro/organization.data:143
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:144
msgid "Release Notes"
msgstr "Sürüm Notları"

#: ../../english/intro/organization.data:146
msgid "CD Images"
msgstr "CD imajları"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "Üretim"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "Test"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:162
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "Destek ve Altyapı"

#: ../../english/intro/organization.data:164
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:171
#, fuzzy
msgid "Buildd administration"
msgstr "Sistem Yönetimi"

#: ../../english/intro/organization.data:189
msgid "Documentation"
msgstr "Belgelendirme"

#: ../../english/intro/organization.data:194
#, fuzzy
msgid "Work-Needing and Prospective Packages list"
msgstr "Üzerinde çalışılması gereken ve Paketlenmesi olası paket listeleri"

#: ../../english/intro/organization.data:196
msgid "Ports"
msgstr "Donanım Platformları"

#: ../../english/intro/organization.data:226
msgid "Special Configurations"
msgstr "Özel Yapılandırmalar"

#: ../../english/intro/organization.data:228
msgid "Laptops"
msgstr "Dizüstüler"

#: ../../english/intro/organization.data:229
msgid "Firewalls"
msgstr "Güvenlik duvarları"

#: ../../english/intro/organization.data:230
msgid "Embedded systems"
msgstr "Gömülü sistemler"

#: ../../english/intro/organization.data:245
msgid "Press Contact"
msgstr "Basınla İletişim"

#: ../../english/intro/organization.data:247
msgid "Web Pages"
msgstr "Web Sayfaları"

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:269
#, fuzzy
msgid "Debian Women Project"
msgstr "Geliştirilen Projeler"

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr "Olaylar"

#: ../../english/intro/organization.data:289
#, fuzzy
msgid "DebConf Committee"
msgstr "Teknik Komite"

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr "İş Ortakları Programı"

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr "Donanım Bağışları Koordinasyonu"

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:329
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:332
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr "Kullanıcı desteği"

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr "Hata Takip Sistemi"

#: ../../english/intro/organization.data:414
#, fuzzy
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Eposta Listeleri Yönetimi"

#: ../../english/intro/organization.data:422
#, fuzzy
msgid "New Members Front Desk"
msgstr "Yeni Geliştiriciler Başvuru Masası"

#: ../../english/intro/organization.data:428
#, fuzzy
msgid "Debian Account Managers"
msgstr "Geliştirici Hesapları Yöneticileri"

#: ../../english/intro/organization.data:432
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Anahtar halkası Yöneticileri (PGP ve GPG)"

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr "Güvenlik Ekibi"

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr "Danışmanlar Sayfası"

#: ../../english/intro/organization.data:453
msgid "CD Vendors Page"
msgstr "CD Satıcıları Sayfası"

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr "Kurallar"

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr "Sistem Yönetimi"

#: ../../english/intro/organization.data:460
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Bu adres Debian makinelerinin herhangi birinde; parola sorunları veya "
"belirli bir paketin kurulumuna olan gereksinim gibi sorunlarla "
"karşılaştığınızda kullanacağınız adrestir."

#: ../../english/intro/organization.data:469
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Debian makineleriyle ilgili donanım sorunları yaşıyorsanız, lütfen <a href="
"\"https://db.debian.org/machines.cgi\">Debian Makineleri</a> sayfasını "
"inceleyin; bu sayfa her makineye ilişkin yönetim bilgilerini içermektedir."

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP Geliştirici Dizinleri Yönetimi"

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr "Yansılar"

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr "DNS Yöneticisi"

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr "Paket Takip Sistemi"

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:488
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:491
#, fuzzy
msgid "Salsa administrators"
msgstr "Alioth yöneticileri"

#~ msgid "Individual Packages"
#~ msgstr "Ayrı paketler"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Yedine yetmişe çocuklar için Debian"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Tıbbî uygulama ve araştırmalar için Debian"

#~ msgid "Debian for education"
#~ msgstr "Eğitim için Debian"

#~ msgid "Debian in legal offices"
#~ msgstr "Resmî dairelerde Debian"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Özürlüler için Debian"

#, fuzzy
#~| msgid "Debian for medical practice and research"
#~ msgid "Debian for science and related research"
#~ msgstr "Tıbbî uygulama ve araştırmalar için Debian"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Eğitim için Debian"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Kurulum Sistemi Ekibi"

#~ msgid "Publicity"
#~ msgstr "Tanıtım"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "``Stable'' Sürüm Yöneticisi"

#~ msgid "Installation System for ``stable''"
#~ msgstr "``Stable'' Kurulum Sistemi"

#~ msgid "Vendors"
#~ msgstr "Satıcılar"

#~ msgid "APT Team"
#~ msgstr "APT Ekibi"

#~ msgid "Handhelds"
#~ msgstr "Elde taşınabilirler"

#~ msgid "Mailing List Archives"
#~ msgstr "Eposta Liste Arşivleri"

#~ msgid "Key Signing Coordination"
#~ msgstr "Anahtar İmzalama Koordinasyonu"

#~ msgid "Accountant"
#~ msgstr "Sayman"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Masaüstünüz için Evrensel İşletim Sistemi"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Kâr amacı gütmeyen kurumlar için Debian"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "İş dünyası için Debian GNU/Linux"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Debian Çokluortam Dağıtımı"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Bu proje henüz resmî bir dahili Debian projesi değil, fakat bu yönde bir "
#~ "niyet bildirilmiş durumda."

#~ msgid "Delegates"
#~ msgstr "Temsilciler"

#~ msgid "Installation"
#~ msgstr "Kurulum"

#~ msgid "Mailing list"
#~ msgstr "Mail listesi"

#~ msgid "Security Audit Project"
#~ msgstr "Güvenlik Duyuruları Projesi"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Güvenlik Ekibi"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth yöneticileri"
