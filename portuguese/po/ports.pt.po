# Brazilian Portuguese translation for Debian website ports.pot
# Copyright (C) 2003-2015 Software in the Public Interest, Inc.
#
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2007
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2012-2015.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"PO-Revision-Date: 2015-07-04 11:56-0300\n"
"Last-Translator: Marcelo Gomes de Santana <marcelo@msantana.eng.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "Contato"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "CPUs"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Créditos"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Desenvolvimento"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Documentação"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Instalação"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr "Configuração"

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Links"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Novidades"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Portando"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Portes"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Problemas"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Mapa de Software"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Estado"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Fornecimento"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Sistemas"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian para Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian para PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "CDs do Hurd"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian para IA-64"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "Debian GNU/NetBSD para i386"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "Debian GNU/NetBSD para Alpha"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "Por quê"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "Pessoas"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian para PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian para Sparc"

#~ msgid "Debian for Laptops"
#~ msgstr "Debian para Laptops"

#~ msgid "Vendor/Name"
#~ msgstr "Vendedor/Nome"

#~ msgid "Date announced"
#~ msgstr "Data anunciada"

#~ msgid "Clock"
#~ msgstr "Relógio"

#~ msgid "ICache"
#~ msgstr "ICache"

#~ msgid "DCache"
#~ msgstr "DCache"

#~ msgid "TLB"
#~ msgstr "TLB"

#~ msgid "ISA"
#~ msgstr "ISA"

#~ msgid "Specials"
#~ msgstr "Especiais"

#~ msgid "No FPU (R2010), external caches"
#~ msgstr "Sem FPU (R2010), cache externo"

#~ msgid "No FPU (R3010)"
#~ msgstr "Sem FPU (R3010)"

#~ msgid "Virtual indexed L1 cache, L2 cache controller"
#~ msgstr "Cache L1 indexado virtual, controlador de cache L2"

#~ msgid "External L1 cache"
#~ msgstr "Cache L1 externo"

#~ msgid "Multiple chip CPU"
#~ msgstr "CPU com múltiplos chips"

#~ msgid ""
#~ "Mips16 isa extension, No FPU, 512k flash, 16k ram, DMAC, UART, Timer, "
#~ "I2C, Watchdog"
#~ msgstr ""
#~ "Extensão isa Mips16, Sem FPU, 512k flash, 12k ram, DMAC, UART, "
#~ "Temporizador, I2C, Watchdog"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, UART, Timer, I2C, LCD Controller"
#~ msgstr ""
#~ "Sem FPU, DRAMC, ROMC, DMAC, UART, Temporizador, I2C, Controlador LCD"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, UART, Timer"
#~ msgstr "Sem FPU, DRAMC, ROMC, DMAC, UART, Temporizador"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, PCIC, UART, Timer"
#~ msgstr "Sem FPU, DRAMC, ROMC, DMAC, PCIC, UART, Temporizador"

#~ msgid "No FPU, SDRAMC, ROMC, Timer, PCMCIA, LCD Controller, IrDA"
#~ msgstr "Sem FPU, SDRAMC, ROMC, Temporizador, PCMCIA, Controlador LCD, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, LCD Controller, IrDA"
#~ msgstr ""
#~ "Sem FPU, SDRAMC, ROMC, UART, Temporizador, PCMCIA, Controlador LCD, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, IrDA"
#~ msgstr "Sem FPU, SDRAMC, ROMC, UART, Temporizador, PCMCIA, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, DMAC, PCIC, UART, Timer"
#~ msgstr "Sem FPU, SDRAMC, ROMC, DMAC, PCIC, UART, Temporizador"

#~ msgid "2 10/100 Ethernet, 4 UART, IrDA, AC'97, I2C, 38 Digital I/O"
#~ msgstr "2 10/100 Ethernet, 4 UART, IrDA, AC'97, I2C, 38 E/S Digital"

#~ msgid "FPU, 32-bit external bus"
#~ msgstr "FPU, barramento externo de 32 bits"

#~ msgid "FPU, 64-bit external bus"
#~ msgstr "FPU, barramento externo de 64 bits"

#~ msgid "FPU, 64-bit external bus, external L2 cache"
#~ msgstr "FPU, barramento externo de 64 bits, cache L2"

#~ msgid "256 L2 cache on die"
#~ msgstr "256 cache L2 on die"

#~ msgid "Mips 16"
#~ msgstr "Mips 16"

#~ msgid ""
#~ "Mips 16, RTC, Keyboard, TouchPanel, Audio, Compact-Flash, UART, Parallel"
#~ msgstr ""
#~ "Mips 16, RTC, Teclado, Painel sensível ao toque, Audio, Compact-Flash, "
#~ "UART, Paralela"

#~ msgid "Mips 16, Compact Flash, UART, Parallel, RTC, Audio, PCIC"
#~ msgstr "Mips 16, Compact Flash, UART, Paralela, RTC, Audio, PCIC"

#~ msgid ""
#~ "Mips 16, LCD controller, Compact Flash, UART, Parallel, RTC, Keyboard, "
#~ "USB, Touchpad, Audio"
#~ msgstr ""
#~ "Mips 16, controlador LCD, Compact Flash, UART, Paralela, RTC, Teclado, "
#~ "USB, Touchpad, Audio"

#~ msgid "libc5-based Debian GNU/FreeBSD"
#~ msgstr "Debian GNU/FreeBSD baseado na libc5"

#~ msgid "Debian for Sparc64"
#~ msgstr "Debian para Sparc64"

#~ msgid "Debian for S/390"
#~ msgstr "Debian para S/390"

#~ msgid "Debian for MIPS"
#~ msgstr "Debian para MIPS"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "Debian para Motorola 680x0"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "Debian GNU/FreeBSD"

#~ msgid "Main"
#~ msgstr "Main (principal)"

#~ msgid "Debian for Beowulf"
#~ msgstr "Debian para Beowulf"

#~ msgid "Debian for ARM"
#~ msgstr "Debian para ARM"

#~ msgid "Debian for AMD64"
#~ msgstr "Debian para AMD64"
