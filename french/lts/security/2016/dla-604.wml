#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans ruby-actionpack-3.2,
un cadriciel de flux et de rendu web qui fait partie de Rails:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7576">CVE-2015-7576</a>

<p>Un défaut a été découvert dans la manière dont le composant Action
Controller comparait les noms et les mots de passe d'utilisateurs lors de
la réalisation d'authentification basique HTTP. Le temps pris pour comparer
les chaînes pourrait différer selon l'entrée, permettant éventuellement à
un attaquant distant de déterminer les noms et les mots de passe
d'utilisateurs valables à l'aide d'une attaque temporelle.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0751">CVE-2016-0751</a>

<p>Un défaut a été découvert dans la manière dont le composant Action Pack
réalisait la recherche de type MIME. Dans la mesure où les requêtes étaient
mises en cache dans un cache global des types MIME, un attaquant pourrait
utilisait ce défaut pour faire grossir le cache indéfiniment, avec comme
conséquence éventuellement un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0752">CVE-2016-0752</a>

<p>Un défaut de traversée de répertoires a été découvert dans la manière
dont le composant Action View recherchait des modèles pour l'affichage. Si
une application passait une entrée non fiable à la méthode <q>render</q>,
un attaquant distant non identifié pourrait utiliser ce défaut pour
afficher des fichiers inattendus et, éventuellement, exécuter du code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2097">CVE-2016-2097</a>

<p>Des requêtes contrefaites pour Action View pourraient avoir pour
conséquence de fournir de fichiers à partir d'emplacements arbitraires, y
compris de fichiers situés en dehors du répertoire de vues de
l'application. Cette vulnérabilité est le résultat d'une correction
incomplète du
<a href="https://security-tracker.debian.org/tracker/CVE-2016-0752">CVE-2016-0752</a>.
Ce bogue a été découvert par Jyoti Singh et Tobias Kraze de Makandra.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2098">CVE-2016-2098</a>

<p>Si une application web ne nettoie pas proprement les entrées
d'utilisateur, un attaquant peut contrôler les arguments de la méthode de
rendu dans un contrôleur ou un affichage, avec comme conséquence la
possibilité d'exécuter du code Ruby arbitraire. Ce bogue a été découvert
par Tobias Kraze de Makandra et joernchen de Phenoelit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6316">CVE-2016-6316</a>

<p>Andrew Carpenter de Critical Juncture a découvert une vulnérabilité de
script intersite affectant Action View. Un texte déclaré comme <q>HTML
safe</q> n'aura pas de guillemets protégés quand il est utilisé comme
valeur d'attribut dans des <q>tag helpers</q>.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.2.6-6+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-actionpack-3.2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-604.data"
# $Id: $
