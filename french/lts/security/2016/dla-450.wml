#use wml::debian::translation-check translation="fce40adff1381ed16a3e5ebae7ad1ff8fcbbb739" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un dépassement de tas a été découvert dans gdk-pixbuf, une bibliothèque
offrant des fonctions de chargement et d'enregistrement d'images, de mise à
l'échelle et de combinaisons d'images en mémoire (« pixbufs »), qui permet à
des attaquants distants de provoquer un déni de service ou éventuellement
d'exécuter du code arbitraire à l'aide d'un fichier BMP contrefait.</p>

<p>Cette mise à jour corrige également un correctif incomplet pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2015-7674">CVE-2015-7674</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7552">CVE-2015-7552</a>

<p>Un dépassement de tas dans la fonction gdk_pixbuf_flip dans
gdk-pixbuf-scale.c dans gdk-pixbuf permet à des attaquants distants de
provoquer un déni de service ou éventuellement d'exécuter du code arbitraire
à l'aide d'un fichier BMP contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7674">CVE-2015-7674</a>

<p>Un dépassement d'entier dans la fonction pixops_scale_nearest dans
pixops/pixops.c dans gdk-pixbuf antérieur à 2.32.1 permet à des attaquants
distants de provoquer un déni de service (plantage de l'application) et
éventuellement d'exécuter du code arbitraire à l'aide d'un fichier d'image
GIF contrefait, ce qui déclenche un dépassement de tas.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.26.1-1+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gdk-pixbuf.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-450.data"
# $Id: $
