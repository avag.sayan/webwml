#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0772">CVE-2016-0772</a>

<p>Une vulnérabilité dans smtplib permet à un attaquant de type « homme du
milieu » (MITM) de réaliser une « StartTLS stripping attack ». smtplib ne
semble pas lever une exception quand la partie terminale (serveur smtp) est
capable d'une négociation avec starttls mais échoue à répondre avec 220
(ok) à un appel explicite de SMTP.starttls(). Cela permet à un « homme du
milieu » malveillant de réaliser une « StartTLS stripping attack » si le
code du client ne vérifie pas explicitement le code de réponse pour
startTLS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5636">CVE-2016-5636</a>

<p>Problème n° 26171 : correction d'un possible dépassement d'entier et
d'une corruption de tas dans zipimporter.get_data().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5699">CVE-2016-5699</a>

<p>Une injection de protocole peut se produire non seulement si une
application règle un en-tête basé sur des valeurs fournies par
l'utilisateur, mais aussi si jamais l'application essaie de récupérer une
URL spécifiée par un attaquant (cas de SSRF – Server Side Request Forgery)
OU si jamais l'application accède à un serveur web malveillant (cas de
redirection).</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.7.3-6+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python2.7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-522.data"
# $Id: $
