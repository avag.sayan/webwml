#use wml::debian::translation-check translation="1284505398735e452bf8d9cbea55c3ba81fc7f8f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans la bibliothèque
d’expressions rationnelles Oniguruma, utilisée notamment dans mbstring de PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19012">CVE-2019-19012</a>

<p>Une dépassement d'entier dans la fonction search_in_range dans regexec.c
conduit à une lecture hors limites, dans laquelle le décalage de lecture est
sous contrôle de l’attaquant (cela affecte seulement la version compilée en
32 bits). Des attaquants distants peuvent causer un déni de service ou une
divulgation d'informations, ou éventuellement provoquer un impact non précisé, à
l'aide d'une expression rationnelle contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19204">CVE-2019-19204</a>

<p>Dans la fonction fetch_range_quantifier dans regparse.c, PFETCH est appelé
sans vérification de PEND. Cela conduit à une lecture hors limites de tampon
basé sur le tas et à un déni de service à l'aide d'une expression
rationnelle contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19246">CVE-2019-19246</a>

<p>Une lecture hors limites de tampon basé sur le tas dans str_lower_case_match
dans regexec.c peut conduire à un déni de service à l'aide d'une expression
rationnelle contrefaite.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.9.5-3.2+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libonig.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2020.data"
# $Id: $
