#use wml::debian::cdimage title="Créer un miroir pour les images de CD de Debian" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="be6f9cbfcdcebe5026a31b4f3171a70f1a2bd9c1" maintainer="Jean-Paul Guillonneau"
# Previous translation by Thomas Huriaux
# Divers translators, see the log file
# Jean-Paul Guillonneau, 2016-19

<p>Pour créer un miroir d'images de CD de Debian, vous avez besoin
d'une machine sous Linux ou sous un système de type Unix avec une connexion
permanente et fiable à l'Internet. Les miroirs d'images de CD de
Debian fournissent des images <tt>.iso</tt> pour des CD ou des
DVD de différentes tailles, les fichiers pour
<a href="https://www.einval.com/~steve/software/jigdo/">jigdo</a>
(<tt>.jigdo</tt> et <tt>.template</tt>), les fichiers
<a href="https://fr.wikipedia.org/wiki/BitTorrent">BitTorrent</a>
(<tt>.torrent</tt>) et les fichiers de vérification des images
(<tt>SHA256SUMS*</tt> et <tt>SHA512SUMS*</tt>).</p>

<toc-display/>

#______________________________________________________________________

<toc-add-entry name="master">Site principal</toc-add-entry>

<p>
<!--
Deux sites sont destinés à la création d'un miroir Debian, un pour
les images de la distribution stable, l'autre pour les images des
distributions bêta, instable et testing.
-->
Les liens du site principal
sont donnés ci-dessous &mdash;&nbsp;cependant, <strong>veuillez</strong>
ne pas négliger la possibilité de créer un miroir à partir d'un autre
miroir proche (pour obtenir la liste des miroirs&nbsp;:
<a href="../http-ftp/">HTTP/FTP</a>, <a href="rsync-mirrors">rsync</a>)
si cela est possible. L'accès au site principal sera certainement
restreint lors du passage vers une nouvelle publication.</p>

<p>Faites également attention au fait qu'une quantité
<strong>énorme</strong> de données sont stockées dans ces répertoires
&mdash;&nbsp;lisez la <a href="#exclude">section ci-dessous</a> pour savoir
comment limiter la taille en excluant certains fichiers.</p>

<ul>

  <li>Images de la distribution stable (mises à jour pour chaque
  nouvelle publication de la distribution stable)&nbsp;:<br/>

      <a href="https://cdimage.debian.org/debian-cd/">\
      <tt>https://cdimage.debian.org/debian-cd/</tt></a><br/>

      <a href="ftp://cdimage.debian.org/debian-cd/">\
      <tt>ftp://cdimage.debian.org/debian-cd/</tt></a><br/>

      <tt>rsync://cdimage.debian.org/debian-cd/</tt>

  </li>
  <li>Images hebdomadaires&nbsp;:<br>

    <a href="https://cdimage.debian.org/cdimage/weekly-builds/">\
    <tt>https://cdimage.debian.org/cdimage/weekly-builds/</tt></a><br>

    <a href="ftp://cdimage.debian.org/cdimage/weekly-builds/">\
    <tt>ftp://cdimage.debian.org/cdimage/weekly-builds/</tt></a><br>

    <tt>rsync://cdimage.debian.org/cdimage/weekly-builds/</tt>

  </li>

  <li>Images quotidiennes&nbsp;:<br>

    <a href="https://cdimage.debian.org/cdimage/daily-builds/">\
    <tt>https://cdimage.debian.org/cdimage/daily-builds/</tt></a><br>

    <a href="ftp://cdimage.debian.org/cdimage/daily-builds/">\
    <tt>ftp://cdimage.debian.org/cdimage/daily-builds/</tt></a><br>

    <tt>rsync://cdimage.debian.org/cdimage/daily-builds/</tt>

  </li>
</ul>
#______________________________________________________________________

<toc-add-entry name="httpftp">Créer un miroir avec FTP et HTTP
n'est pas recommandé</toc-add-entry>

<p>Vous ne devez pas utiliser FTP ou HTTP pour mettre à jour votre miroir.
Ces techniques de transfert présentent une probabilité d'échec
importante du fait de la taille énorme des fichiers.</p>

<p>De plus, HTTP et FTP ne permettent pas la vérification de l'intégrité
des données téléchargées, et il est plus que probable que les
téléchargements interrompus ou les données corrompues ne seront pas
signalés.</p>
#______________________________________________________________________

<toc-add-entry name="download">Créer un miroir avec rsync est
tolérable</toc-add-entry>

<p>Le programme <a href="http://rsync.samba.org/"><kbd>rsync</kbd></a>
est une bonne solution pour la création d'un miroir. Elle est moins
efficace que l'autre, à savoir la création d'un miroir à la manière de Debian
présentée ci-dessous, mais peut être plus facile à configurer.
De plus, elle assure que l'ensemble des fichiers sont transférés correctement
et que les métadonnées (par exemple les horodatages) sont synchronisées
de la même manière que les données des fichiers.</p>

<p>Reportez-vous à la section <a href="#exclude">exclure des fichiers
du miroir</a> pour des exemples d'utilisation de <kbd>--include</kbd>
et <kbd>--exclude</kbd>. La <a href="rsync-mirrors">liste des miroirs
rsync</a> est disponible sur une page séparée.</p>

<p>Veuillez utiliser au moins les options <strong><kbd>--times
--links --hard-links --partial --block-size=8192</kbd></strong>.
Cela conservera la date de dernière modification, les liens symboliques
et durs, et un bloc de 8192 octets (le plus adapté pour les images de
CD) sera utilisé. Lorsque la date de dernière modification et la
taille d'un fichier n'ont pas été modifiées, <kbd>rsync</kbd> ignore le
fichier, aussi <tt>--times</tt> est réellement nécessaire.</p>
#______________________________________________________________________

<toc-add-entry name="jigdolite">Créer un miroir avec jigdo-lite
n'est pas recommandé</toc-add-entry>

<p>Les versions récentes du programme
<a href="https://www.einval.com/~steve/software/jigdo/">\
<kbd>jigdo-lite</kbd></a> prennent en charge le téléchargement groupé de
plusieurs images. Cependant, nous ne recommandons pas d'utiliser
<kbd>jigdo-lite</kbd> pour la création des miroirs de CD Debian
&mdash;&nbsp;veuillez utiliser <kbd>jigdo-mirror</kbd> à la place.</p>
#______________________________________________________________________

<toc-add-entry name="jigdomirror">Créer un miroir avec jigdo-mirror est
recommandé</toc-add-entry>

<p>En fait, cela signifie&nbsp;: créer un miroir des fichiers <tt>.iso</tt>
en utilisant
<a href="https://www.einval.com/~steve/software/jigdo/"><kbd>jigdo-mirror</kbd></a>,
puis (si vous voulez créer un miroir d'autres types de fichiers, par exemple
les fichiers <tt>.jigdo</tt> et <tt>.template</tt>), lancer rsync sur le
répertoire pour récupérer le reste. Les scripts disponibles sur
<a href="http://www.acc.umu.se/~maswan/debian-push/cdimage/">cette
page</a> peuvent vous aider à configurer l'ensemble.</p>

<p>De nombreuses personnes tiennent à jour des miroirs
«&nbsp;classiques&nbsp;» de Debian (<kbd>debian/</kbd>) ou ont un tel
miroir à proximité. Cela signifie qu'ils ont déjà les fichiers .deb qui
sont dans les images de CD et DVD. La question évidente est&nbsp;: pourquoi
ne pas utiliser ces fichiers dans les images de CD ou DVD&nbsp;?</p>

<p><kbd>jigdo-mirror</kbd> est un programme qui permet de fabriquer des
images de CD et DVD de Debian à l'aide des fichiers d'un miroir
«&nbsp;classique&nbsp;», ainsi que de quelques fichiers modèles
supplémentaires à l'usage de jigdo.</p>

<p>Tout d'abord, vous avez besoin des fichiers modèles pour jigdo.
Voyez la <a href="../jigdo-cd">page d'information de jigdo</a> pour
des liens. Récupérez les fichiers de toutes les architectures pour
lesquelles vous souhaitez fabriquer les images.</p>

<p>Créez un fichier <kbd>~/.jigdo-mirror</kbd> pour configurer le programme.
Voici un exemple&nbsp;:</p>

<pre>
jigdoDir="/chemin/vers/votre/miroir/debian-cd/current/jigdo"
imageDir="/chemin/vers/votre/miroir/debian-cd/current/images"
tmpDir="/chemin/vers/votre/miroir/debian-cd/current/images"
debianMirror="file:/chemin/vers/votre/miroir/debian"
include='i386/|sparc/|powerpc/|source/'; exclude='-1\.'
</pre>

<p>Les variables <i>include</i> et <i>exclude</i> font référence à la liste
des architectures (sous forme d'expressions rationnelles) pour lesquelles
vous souhaitez créer des images. Pour plus d'informations, veuillez
consulter le manuel de <kbd>jigdo-mirror</kbd> ou le code source (c'est
un script shell comportant de nombreux commentaires).</p>

<p>Une fois l'étape de configuration terminée, lancez simplement
<kbd>jigdo-mirror</kbd> et il fera tout tout seul. Il affiche beaucoup
de messages et l'exécution prend un certain temps, aussi nous vous
suggérons de prendre des mesures pour le gérer (lancez le programme avec screen,
redirigez la sortie vers un fichier, etc.).</p>

<toc-add-entry name="pushmirror">Comment devenir un miroir
«&nbsp;Push&nbsp;»&nbsp;?</toc-add-entry>

<p>À chaque fois que de nouvelles images sont disponibles, le site
principal peut envoyer un message à ses miroirs et les faire immédiatement
démarrer la mise à jour. De cette manière, les nouvelles données sont
«&nbsp;poussées&nbsp;» plutôt que «&nbsp;tirées&nbsp;» par les miroirs
durant la mise à jour quotidienne suivante, ce qui conduit à une
propagation plus rapide des nouvelles publications d'une image.</p>

<p>Si vous voulez que votre miroir fasse partie de ce système, veuillez
consulter <a href="http://www.acc.umu.se/~maswan/debian-push/cdimage/">\
cette page</a>.</p>
#______________________________________________________________________

<toc-add-entry name="exclude">Exclure des fichiers du miroir</toc-add-entry>

<p>Pour réduire la place nécessaire pour votre miroir de CD Debian,
vous pouvez exclure certains fichiers du processus de création du miroir.
Les instructions suivantes présentent les options courantes pour
<kbd>rsync</kbd>, mais peuvent vous aider si vous utilisez un utilitaire
différent pour la création des miroirs. Avec <kbd>rsync</kbd>, les
options <kbd>--include</kbd> et <kbd>--exclude</kbd> sont prises en
compte en fonction de leur ordre d'apparition, et la première option
dont le motif du fichier correspond détermine si le fichier doit être
inclus ou exclu.</p>

<ul>

  <li><strong>Exclure le code source&nbsp;:</strong>
  <kbd>--exclude=source/</kbd><br>

  Évite aux images contenant du code source d'être incluses dans
  le miroir. Veuillez noter que certaines personnes considèrent inapproprié
  d'offrir des binaires d'un programme sous licence GPL sur un serveur
  sans offrir en même temps les codes source du programme <em>sur le même
  serveur</em>.</li>

  <li><strong>Exclure les images entières&nbsp;:</strong>
  <kbd>--include='*netinst*.iso'
  --exclude='*.iso'</kbd><br>

  Exclut toutes les images de CD et DVD pour toutes les architectures
  <em>à l'exception</em> des images <tt>.iso</tt> pour l'installation par
  le réseau. Nous recommandons de toujours inclure ces images&nbsp;: en
  considération de leur taille, elles sont extrêmement utiles&nbsp;!</li>

  <li><strong>Exclure les images entières des architectures
  non-i386&nbsp;:</strong> <kbd>--include='*netinst*.iso'
  --include='i386/**.iso' --exclude='*.iso'</kbd><br>

  Comme ci-dessus, mais <em>inclut</em> toutes les images de CD et
  de DVD pour l'architecture i386.</li>

  <li><strong>Exclure les images entières, à l'exception des trois premiers
  CD de l'architecture i386&nbsp;:</strong>
  <kbd>--include='*netinst*.iso' --include='i386/**-[1-3].iso'
  --exclude='*.iso'</kbd><br>

  L'ensemble complet des images i386 peut toujours prendre trop d'espace
  pour vous si cela inclut les images DVD simple et double
  couche. Cela exclut l'ensemble des images <tt>.iso</tt> à l'exception
  des images pour l'installation par le réseau ainsi que les trois premiers
  DVD pour l'architecture i386.</li>

  <li><strong>Exclure plusieurs architectures à l'exception de
  i386&nbsp;:</strong>
  <kbd>--exclude=alpha/ --exclude=arm/ --exclude=hppa/ --exclude=hurd/
  --exclude=ia64/ --exclude=m68k/ --exclude=mips/ --exclude=mipsel/
  --exclude=powerpc/ --exclude=s390/ --exclude=sh/
  --exclude=sparc/</kbd><br>

  Inclut seulement l'ensemble complet des fichiers pour l'architecture i386,
  et n'inclut aucun des fichiers <tt>.jigdo</tt>, <tt>.iso</tt>, etc., pour
  les autres architectures.<br>

<strong>Veuillez vérifier la liste des architectures avant la création du miroir
— les modifications de liste et ces exemples peuvent être obsolètes !</strong></li>

</ul>
#______________________________________________________________________

<toc-add-entry name="names">Conventions de nommage et taille nécessaire
pour les images <tt>.iso</tt></toc-add-entry>

<p>Les différents types d'images <tt>.iso</tt> sont différentiables
par leurs noms. Cela vous permet de restreindre votre miroir à certains
types d'images.</p>

<ul>

  <li><strong><tt>*-netinst.iso</tt></strong>&nbsp;: une image pour
  chacune des architectures, jusqu’à 500&nbsp;Mo.</li>

  <li><strong><tt>*-dvd.iso</tt></strong> (DVD simple couche)&nbsp;:
 plusieurs images, chacune pouvant atteindre 4482&nbsp;Mo. Pour
 <em>Buster</em>, il existe <strong>16</strong> images de DVD par architecture.
 Les serveurs de Debian ne fournissent seulement qu’un petit sous-ensemble
 d’images de DVD sous la forme de .iso pour un téléchargement direct : trois
 pour amd64, trois pour i386 et une pour chacune des autres architectures. Les
 images restantes sont uniquement fournies sous la forme de jigdo.</li>

  <li><strong><tt>*-bd.iso</tt></strong> (Blu-ray simple couche) : comme
  ci-dessus, si ce n'est que les images individuelles peuvent atteindre
  23&nbsp;Go. Ces images sont seulement disponibles sous forme de fichiers jigdo
  pour un ensemble limité d’architectures (amd64 et i386) et de sources.</li>

  <li><strong><tt>*-dlbd.iso</tt></strong> (Blu-ray double couche)&nbsp;:
  comme ci-dessus, si ce n'est que les images individuelles peuvent atteindre
  48&nbsp;Go. Ces images sont seulement disponibles sous forme de
  fichiers jigdo pour un ensemble limité d’architectures (amd64 et i386) et de
  sources.</li>

  <li><strong><tt>*-STICK16GB*.iso</tt></strong> (images 16 Go USB)&nbsp;:
  comme ci-dessus, si ce n'est que les images individuelles peuvent atteindre
  16&nbsp;Go. Ces images sont seulement disponibles sous forme de
  fichiers jigdo pour un ensemble limité d’architectures (amd64 et i386) et de
  sources.</li>

</ul>
#______________________________________________________________________

<toc-add-entry name="register">Enregistrer le miroir</toc-add-entry>

<p>Afin de rendre votre miroir d'images de CD utilisable par un plus
grand nombre d'utilisateurs, vous pouvez l'enregistrer sur notre liste
de miroirs tels que <a href="../http-ftp/">celle-ci</a> ou
<a href="rsync-mirrors">celle-là</a>. Cependant, étant donné que
les images complètes sont des fichiers de grande taille, cela peut
entraîner un trafic quotidien de plusieurs gigaoctets.</p>

<p>Vous pouvez enregistrer votre miroir soit en remplissant le
<a href="$(HOME)/mirror/submit">formulaire pour les miroirs</a>
(veuillez noter que les champs CDImage-* sont les plus importants),
soit en envoyant un courriel à
<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;debian-cd&#64;lists.debian.org">\
debian-cd&#64;lists.debian.org</a>.</p>

<p>Nous apprécions tous les nouveaux miroirs d'images de CD.
Par avance, merci&nbsp;!</p>
