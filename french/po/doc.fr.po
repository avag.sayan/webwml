# Translation of docs web pages to French
# Copyright (C) 2002-2005, 2007-2010, 2012, 2014 Debian French l10n team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the Debian web site.
#
# Denis Barbier, 2002, 2004.
# Pierre Machard, 2002-2004.
# Frederic Bothamy, 2004, 2005, 2007.
# Thomas Huriaux, 2005.
# Mohammed Adnène Trojette, 2005.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2008, 2009.
# David Prévot <david@tilapin.org>, 2010, 2012, 2014.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fichier>, 2017.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2018.
# Alban Vidal <alban.vidal@zordhak.fr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2019-03-30 17:56+0100\n"
"Last-Translator: Alban Vidal <alban.vidal@zordhak.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "Auteur :"

#: ../../english/doc/books.def:41
msgid "Debian Release:"
msgstr "Version de Debian :"

#: ../../english/doc/books.def:44
msgid "email:"
msgstr "Courrier électronique :"

#: ../../english/doc/books.def:48
msgid "Available at:"
msgstr "Disponible à :"

#: ../../english/doc/books.def:51
msgid "CD Included:"
msgstr "CD inclus :"

#: ../../english/doc/books.def:54
msgid "Publisher:"
msgstr "Éditeur :"

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "Auteurs :"

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "Éditeurs :"

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "Responsable :"

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "État :"

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "Disponibilité :"

#: ../../english/doc/manuals.defs:97
msgid "Latest version:"
msgstr "Dernière version :"

#: ../../english/doc/manuals.defs:113
msgid "(version <get-var version />)"
msgstr "(version <get-var version />)"

#: ../../english/doc/manuals.defs:143 ../../english/releases/arches.data:38
msgid "plain text"
msgstr "texte seul"

#: ../../english/doc/manuals.defs:159 ../../english/doc/manuals.defs:169
#: ../../english/doc/manuals.defs:193
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""
"Les dernières sources au format <get-var srctype /> sont disponibles dans le "
"dépôt <a href=\"https://packages.debian.org/git\">Git</a>."

#: ../../english/doc/manuals.defs:161 ../../english/doc/manuals.defs:171
#: ../../english/doc/manuals.defs:179 ../../english/doc/manuals.defs:187
#: ../../english/doc/manuals.defs:195
msgid "Web interface: "
msgstr "Interface web : "

#: ../../english/doc/manuals.defs:162 ../../english/doc/manuals.defs:172
#: ../../english/doc/manuals.defs:180 ../../english/doc/manuals.defs:188
#: ../../english/doc/manuals.defs:196
msgid "VCS interface: "
msgstr "Interface du système de gestion de versions : "

#: ../../english/doc/manuals.defs:177
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/cvs\">Cvs</a> repository."
msgstr ""
"Les dernières sources au format <get-var srctype /> sont disponibles dans le "
"dépôt <a href=\"https://packages.debian.org/cvs\">CVS</a>."

#: ../../english/doc/manuals.defs:185
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/subversion\">Subversion</a> repository."
msgstr ""
"Les dernières sources au format <get-var srctype /> sont disponibles dans le "
"dépôt <a href=\"https://packages.debian.org/subversion\">Subversion</a>."

#: ../../english/doc/manuals.defs:203
msgid ""
"CVS sources working copy: set <code>CVSROOT</code>\n"
"  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
"  and check out the <kbd>boot-floppies/documentation</kbd> module."
msgstr ""
"Copie de travail des sources CVS : positionnez <code>CVSROOT</code> à <kbd>:"
"ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>, et récupérez le "
"module <kbd>boot-floppies/documentation</kbd>."

#: ../../english/doc/manuals.defs:208
msgid "CVS via web"
msgstr "Accès CVS par le web"

#: ../../english/doc/manuals.defs:212 ../../english/doc/manuals.defs:216
msgid "Debian package"
msgstr "Paquet Debian"

#: ../../english/doc/manuals.defs:221 ../../english/doc/manuals.defs:225
msgid "Debian package (archived)"
msgstr "Paquet Debian (archivé)"

#: ../../english/doc/books.data:32
msgid ""
"\n"
"  Debian 9 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""
"\n"
"  Debian 9 est le guide indispensable pour apprendre Linux. Il démarre\n"
"  au niveau néophyte et apprend à déployer un système avec une interface\n"
"  graphique et un terminal. Ce livre fournit les connaissances de base\n"
"  pour s’améliorer et devenir un administrateur système « junior ».\n"
"  Commencez par l’exploration de l’interface du bureau GNOME et son\n"
"  ajustement aux besoins personnels. Surmontez votre peur d’utiliser le\n"
"  terminal Linux et apprenez les commandes essentielles pour administrer\n"
". Debian. Améliorez votre savoir à propos des services système (systemd)\n"
"  et apprenez à les adapter. Utilisez mieux les logiciels, dans et hors de\n"
"  Debian. Gérez votre réseau personnel avec network-manager, etc. Dix pour\n"
"  cent des bénéfices de ce livre seront reversés au projet Debian."

#: ../../english/doc/books.data:61 ../../english/doc/books.data:171
#: ../../english/doc/books.data:226
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""
"Écrit par deux développeurs Debian, ce livre libre a commencé comme une\n"
"  traduction de leur succès de librairie « Cahiers de l'Admin » (édité chez\n"
"  Eyrolles). Accessible à tous, ce livre enseigne l'essentiel à tous ceux\n"
"  désirant devenir administrateurs Debian GNU/Linux efficaces et autonomes.\n"
"  Il couvre tous les aspects qu'un administrateur Linux devrait maîtriser,\n"
"  depuis l'installation et la mise à jour du système, jusqu'à la création\n"
"  de paquets et la compilation du noyau, mais aussi la supervision,\n"
"  sauvegarde et migration, sans oublier les sujets avancés comme la\n"
"  configuration de SELinux pour sécuriser les services, les installations\n"
"  automatisées ou la virtualisation avec Xen, KVM ou LXC."

#: ../../english/doc/books.data:83
msgid ""
"The aim of this freely available, up-to-date, book is to get you up to\n"
"  speed with Debian (including both the current stable release and the\n"
"  current unstable distribution). It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school)."
msgstr ""
"L'objectif de ce livre librement disponible et à jour est de vous\n"
"  familiariser rapidement avec Debian (y compris avec la version stable\n"
"  actuelle et avec la distribution instable actuelle). Il est complet avec\n"
"  une prise en charge de base pour l'utilisateur qui installe et maintient\n"
"  le système lui-même que ce soit à la maison, au bureau, dans un club ou\n"
"  une école."

#: ../../english/doc/books.data:105
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""
"Le premier livre en français à propos de Debian en est déjà à sa\n"
"  cinquième édition. Il couvre tous les aspects de l'administration de\n"
"  Debian depuis l'installation jusqu'à la configuration des services\n"
"  réseau. Écrit par deux développeurs Debian, ce livre peut intéresser un\n"
"  grand  nombre de personnes&nbsp;: le débutant désirant découvrir Debian,\n"
"  l'utilisateur avancé cherchant des astuces pour améliorer sa maîtrise\n"
"  des outils Debian et l'administrateur qui veut construire un réseau\n"
"  fiable avec Debian."

#: ../../english/doc/books.data:125
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""
"Le livre couvre des sujets allant du concept de gestion de paquets à\n"
"  l’aide des outils disponibles jusqu’à comment les utiliser pour des\n"
"  problèmes qui peuvent se produire concrètement et comment les\n"
"  résoudre. Le livre est écrit en allemand, une traduction anglaise\n"
"  étant prévue. Format : livre numérique (en ligne, HTML, PDF, ePub, Mobi), "
"un livre\n"
"  imprimé est prévu.\n"

#: ../../english/doc/books.data:146
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""
"Ce livre vous enseigne la façon d'installer et configurer le système et "
"aussi la façon d'utiliser Debian en environnement professionnel. Il montre "
"tout le potentiel de la distribution (dans sa version actuelle 8) et fournit "
"un manuel pratique pour tous les utilisateurs qui veulent en apprendre plus "
"sur Debian et l'éventail de services proposés."

#: ../../english/doc/books.data:200
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""
"Ce livre, écrit par deux chercheurs en pénétration – Annihilator et\n"
"  Firstblood – vous enseigne la façon de construire et de configurer\n"
"  un serveur Debian 8.x pour durcir la sécurité du système en utilisant\n"
"  de concert Kali Linux et Debian. DNS, FTP, SAMBA, DHCP, serveur web\n"
"  Apache2, etc., sont abordés. Partant du principe que « l'origine\n"
"  de Kali Linux est Debian », il explique comment améliorer la sécurité de\n"
"  Debian en appliquant la méthode des tests de pénétration. Le livre couvre\n"
"  diverses questions de sécurité comme les certificats SSL, le\n"
"  pare-feu UFW, les vulnérabilités de MySQL, l'anti-virus commercial "
"Symantec, y\n"
"  compris le système de détection d'intrusion Snort."

#: ../../english/releases/arches.data:36
msgid "HTML"
msgstr "HTML"

#: ../../english/releases/arches.data:37
msgid "PDF"
msgstr "PDF"

#~ msgid ""
#~ "The latest <get-var srctype /> source is available through the <a href="
#~ "\"https://www.debian.org/doc/cvs\">Subversion</a> repository."
#~ msgstr ""
#~ "Les dernières sources au format <get-var srctype /> sont disponibles dans "
#~ "le <a href=\"https://www.debian.org/doc/cvs\">dépôt Subversion</a>."
