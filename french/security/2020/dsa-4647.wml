#use wml::debian::translation-check translation="9e2bd6fdfda60b03320893da8be2d9ad75fedff5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Il a été signalé que l'implémentation des profils HID et HOGP de BlueZ
n'exige pas de façon particulièrement de lien entre le périphérique et
l'hôte. Des périphériques malveillants peuvent tirer avantage de ce défaut
pour se connecter à un hôte cible et se faire passer pour un périphérique
HID existant sans sécurité ou de faire se produire une découverte de
services SDP ou GATT, ce qui permettrait l'injection de rapports HID dans
le sous-système d'entrée à partir d'une source non liée.</p>

<p>Une nouvelle option de configuration (ClassicBondedOnly) pour le profil
HID est introduite pour s'assurer que les connexions d'entrées ne viennent
que de connexions avec des périphériques liés. Les options sont par défaut
à <q>false</q> pour maximiser la compatibilité avec les périphériques.</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 5.43-2+deb9u2.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 5.50-1.2~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bluez.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bluez, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bluez">\
https://security-tracker.debian.org/tracker/bluez</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4647.data"
# $Id: $
