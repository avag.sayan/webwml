<define-tag pagetitle>Debian Edu / Skolelinux Buster — Eine vollständige Linux-Lösung für Ihre Schule</define-tag>
<define-tag release_date>2019-07-07</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="df8b497185c1a26f20b79e08fe7543e9c4d55353" maintainer="Wolfgang Schweer"

<p>
Müssen Sie einen Computerraum oder ein komplettes Schulnetzwerk betreuen?
Wollen Sie Server, Arbeitsplatzrechner und Laptops einrichten, die dann
zusammenarbeiten?
Möchten Sie die Stabilität von Debian kombiniert mit schon vorkonfigurierten
Netzwerk-Diensten?
Wünschen Sie eine Weboberfläche zum Verwalten von Systemen und hunderten
oder noch mehr Benutzerkonten?
Haben Sie sich gefragt, ob und was mit älteren Computern noch anzufangen ist?
</p>

<p>
Dann ist Debian Edu richtig für Sie. Die Lehrer selbst (oder ihr technischer
Support) können innerhalb von einigen Tagen eine vollständige Lernumgebung für
viele Benutzer und mit vielen Rechnern einrichten. Debian Edu kommt mit
hunderten vorinstallierter Anwendungen, weitere Debian-Pakete können jederzeit
hinzugefügt werden.
</p>

<p>
Das Debian Edu Entwicklerteam freut sich, die Veröffentlichung von Debian Edu 10 <q>Buster</q>
bekanntzugeben; es beruht auf Debian 10 <q>Buster</q>.
Es wäre gut, wenn Sie diese Veröffentlichung testen und uns eine Rückmeldung
(&lt;debian-edu@lists.debian.org&gt;) geben könnten, damit wir es weiter verbessern können.
</p>

<h2>Über Debian Edu und Skolelinux</h2>

<p>
<a href="https://wiki.debian.org/DebianEdu"> Debian Edu, auch unter dem Namen
Skolelinux bekannt</a>, ist eine auf Debian basierende Distribution, die
eine gebrauchsfertige Umgebung für ein komplett konfiguriertes Schulnetzwerk
bereit stellt. Unmittelbar nach der Installation steht ein Schulserver zur
Verfügung, auf dem alle für ein Schulnetzwerk notwendigen Dienste laufen.
Es müssen nur noch Benutzer und Maschinen hinzugefügt werden, was mit der
komfortablen Weboberfläche von GOsa² erledigt wird. Eine Umgebung für das Booten
über das Netzwerk steht ebenfalls zur Verfügung; damit können nach der
anfänglichen Installation des Hauptservers von CD / DVD / BD oder USB-Stick
alle anderen Rechner über das Netz installiert werden.
Ältere Computer (bis zu zehn Jahre alt oder sogar älter) können als LTSP
Thin Clients oder Diskless Workstations eingesetzt werden; diese booten
über das Netzwerk und benötigen überhaupt keine Installation oder Konfiguration.
Der Debian Edu Schulserver bietet Authentifizierung mittels LDAP-Verzeichnis
und Kerberos, zentralisierte Benutzerverzeichnisse, DHCP-Server, Web-Proxy und
viele weitere Dienste. Der Desktop enthält mehr als 60 unterrichtsbezogene
Software-Pakete, weitere stehen im Debian-Depot zur Verfügung. Schulen können
zwischen Xfce, GNOME, LXDE, MATE, KDE Plasma and LXQt als graphischer
Arbeitsumgebung wählen.
</p>

<h2>Neuerungen in Debian Edu 10 <q>Buster</q></h2>

<p>Einige Punkte der Veröffentlichungs-Informationen für Debian Edu 10 <q>Buster</q>,
das auf Debian 10 <q>Buster</q> beruht.
Die vollständige Liste inclusive weiterer Informationen gibt es im
(englischprachigen) <a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Features#New_features_in_Debian_Edu_Buster">Debian Edu manual chapter</a> bzw. entsprechend im <a href="https://jenkins.debian.net/userContent/debian-edu-doc/debian-edu-doc-de/debian-edu-buster-manual.html#Features">Debian Edu Handbuch</a>.
</p>

<ul>
<li>
Es gibt nun offizielle Debian Installation-Images.
</li>
<li>
Eine an örtliche Gegebenheiten angepasste Installation ist möglich.
</li>
<li>
Es gibt zusätzliche Meta-Pakete; diese gruppieren die unterrichtsbezogenen
Software-Pakete nach Schulstufen.
</li>
<li>
Verbesserte Anpassung für alle von Debian unterstützten Sprachen.
</li>
<li>
Einfachere Einrichting einer an die örtlichen Gegebenheiten angepassten
multi-lingualen Arbeitsumgebung.
</li>
<li>
Ein GOsa²-Plugin für die Passwortverwaltung wurde hinzugefügt.
</li>
<li>
Verbesserte Unterstützung für TLS/SSL innerhalb des internen Netzwerks.
</li>
<li>
Das Kerberos-Setup unterstützt NFS und SSH.
</li>
<li>
Ein Werkzeug zum erneuten Einrichten des LDAP-Verzeichnisses ist verfügbar.
</li>
<li>
Auf allen Systemen mit dem LTSP-Server-Profil ist der X2Go-Server installiert.
</li>
</ul>

<h2>Download-Optionen, Installationsschritte und Handbuch</h2>

<p>
Separate Netzwerk-Installer CD-Images für 64-Bit- und 32-Bit-PCs
sind verfügbar. Nur in seltenen Fällen (für PCs älter als etwa 12 Jahre) wird 
das 32-Bit-Image erforderlich sein. Die Images können hier heruntergeladen
werden:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Alternativ gibt es auch erweiterte BD-Images (mit einer Größe
von mehr als 5 GB). Damit ist es möglich, auch ohne Internetverbindung ein
vollständiges Debian-Edu-Netzwerk einzurichten - für alle unterstützten
Arbeitsumgebungen und alle von Debian unterstützten Sprachen. Diese Images
können hier heruntergeladen werden:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
Die Images können mittels der im Downloadverzeichnis vorhandenen signierten
Checksums verifiziert werden.
<br />
Sobald ein Image heruntergeladen wurde, kann geprüft werden, ob
</p>

<ul>
<li>
dessen Checksum mit derjenigen in der angegebenen Datei übereinstimmt, und ob
</li>
<li>
die Datei mit den Checksums nicht manipuliert wurde.
</li>
</ul>

<p>
Um mehr Informationen über diese Schritte zu bekommen, lesen Sie den (englischsprachigen)
<a href="https://www.debian.org/CD/verify">verification guide</a>.
</p>

<p>
Da Debian Edu 10 <q>Buster</q> vollständig auf Debian 10 <q>Buster</q> beruht,
stehen die Quellen aller Pakete im Debian-Depot zur Verfügung.
</p>

<p>
Bitte beachten Sie die (englischprachige) Statusseite:
<a href="https://wiki.debian.org/DebianEdu/Status/Buster">Debian Edu Buster status page</a>.
Diese enthält aktualisierte Information über Debian Edu 10 <q>Buster</q> sowie
Hinweise, wie <code>rsync</code> zum Herunterladen der ISO-Images verwendet
werden kann.
</p>

<p>
Wenn Sie ein Upgrade von Debian Edu 9 <q>Stretch</q> durchführen, lesen Sie
bitte den zugehörigen Abschnitt im (englischprachigen) <a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Upgrades">Debian Edu manual chapter</a> bzw. den entsprechenden Abschnitt im deutschen Handbuch.
</p>

<p>
Bitte lesen Sie vor der Installation den entsprechenden (englischprachigen) Handbuchabschnitt:
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Installation#Installing_Debian_Edu">Debian Edu manual chapter</a>
bzw. den entsprechenden Abschnitt im deutschen Handbuch.
</p>

<p>
Nach der Installation müssen diese ersten Schritte erfolgen:
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/GettingStarted">first steps</a>.
Einen entsprechenden Abschnitt gibt es auch im deutschen Handbuch.
</p>

<p>
Unter <a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/">Debian Edu wiki pages</a>
gibt es die neueste Version des Debian Edu <q>Buster</q> Handbuchs in englischer Sprache.
Das Benutzerhandbuch für Debian Edu <q>Buster</q> wurde vollständig ins Deutsche,
Französische, Italienische, Dänische, Niederländische, in Norwegisch Bokmål und ins Japanische
übersetzt. Teilweise übersetzte Versionen gibt es für Spanisch und Vereinfachtes Chinesisch.
Auf jenkins.debian.net gibt es die neuesten übersetzten Versionen des Handbuchs; siehe
<a href="https://jenkins.debian.net/userContent/debian-edu-doc/">die Übersicht</a>.
</p>

<p>
Weitere Informationen über Debian 10 <q>Buster</q> selbst gibt es in den
Veröffentlichungs-Informationen und in der Installationsanleitung, zu finden unter
<a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Über Debian</h2>

<p>
Das Debian-Projekt ist ein Zusammenschluss von Entwicklern freier Software,
die Ihre Zeit und Anstrengungen einbringen, um ein vollständig freies
Betriebssystem zu erstellen, welches als Debian bekannt ist.
</p>

<h2>Kontaktinformationen</h2>

<p>
Weitere Informationen finden Sie auf der Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a> oder senden Sie eine
E-Mail (auf Englisch) an &lt;press@debian.org&gt;.
</p>

