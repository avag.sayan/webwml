# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Gerfried Fuchs <alfie@ist.org>, 2004.
# Dr. Tobias Quathamer <toddy@debian.org>, 2004.
# Holger Wansing <linux@wansing-online.de>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml ports\n"
"PO-Revision-Date: 2012-09-29 23:23+0200\n"
"Last-Translator: Holger Wansing <linux@wansing-online.de>\n"
"Language-Team: Debian l10n German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian für Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian für PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "Hurd-CDs"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian für IA-64"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "Kontakt"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "CPUs"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Danksagung"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Entwicklung"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Dokumentation"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Installation"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr "Konfiguration"

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Links"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Neues"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Portierung"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Portierungen"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Probleme"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Software-Verzeichnis"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Status"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Bezugsquellen"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Systeme"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "Debian GNU/NetBSD für i386"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "Debian GNU/NetBSD für Alpha"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "Wieso"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "Leute"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian für PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian für Sparc"

#~ msgid "Debian for AMD64"
#~ msgstr "Debian für AMD64"

#~ msgid "Debian for ARM"
#~ msgstr "Debian für ARM"

#~ msgid "Debian for Beowulf"
#~ msgstr "Debian für Beowulf"

#~ msgid "Main"
#~ msgstr "Hauptseite"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "Debian GNU/FreeBSD"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "Debian für Motorola 680x0"

#~ msgid "Debian for MIPS"
#~ msgstr "Debian für MIPS"

#~ msgid "Debian for S/390"
#~ msgstr "Debian für S/390"

#~ msgid "Debian for Sparc64"
#~ msgstr "Debian für Sparc64"

#~ msgid "Debian for Laptops"
#~ msgstr "Debian für Laptops"
