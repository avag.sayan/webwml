#use wml::debian::translation-check translation="122cd4d0371114e8cb5f8eb2f4d7b3d228204e1e"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el motor web webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8625">CVE-2019-8625</a>

    <p>Sergei Glazunov descubrió que contenido web preparado maliciosamente
    puede dar lugar a ejecución de scripts entre sitios universal («universal cross site scripting»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8720">CVE-2019-8720</a>

    <p>Wen Xu descubrió que contenido web preparado maliciosamente puede dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8769">CVE-2019-8769</a>

    <p>Pierre Reimertz descubrió que la visita de un sitio web preparado
    maliciosamente puede revelar el historial de navegación.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8771">CVE-2019-8771</a>

    <p>Eliya Stein descubrió que contenido web preparado maliciosamente puede
    violar la política de entorno aislado para iframes («iframe sandboxing policy»).</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.26.1-3~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de webkit2gtk.</p>

<p>Para información detallada sobre el estado de seguridad de webkit2gtk, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4558.data"
