#use wml::debian::translation-check translation="165625c3a669960bb5e1c766db812564b5fd665e"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron múltiples vulnerabilidades en la implementación del
lenguaje de programación Perl. El proyecto «Vulnerabilidades y exposiciones comunes» («Common Vulnerabilities and
Exposures») identifica los problemas siguientes:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18311">CVE-2018-18311</a>

    <p>Jayakrishna Menon y Christophe Hauser descubrieron una vulnerabilidad
    de desbordamiento de entero en Perl_my_setenv que da lugar a un desbordamiento de
    memoria dinámica («heap») con entrada controlada por el atacante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18312">CVE-2018-18312</a>

    <p>Eiichi Tsukata descubrió que una expresión regular manipulada podría
    provocar un desbordamiento de memoria dinámica («heap») en escritura en tiempo de compilación,
    permitiendo, potencialmente, la ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18313">CVE-2018-18313</a>

    <p>Eiichi Tsukata descubrió que una expresión regular manipulada podría
    provocar un desbordamiento de memoria dinámica («heap») en lectura en tiempo de compilación, dando
    lugar a fuga de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18314">CVE-2018-18314</a>

    <p>Jakub Wilk descubrió que una expresión regular preparada de una manera determinada
    podría dar lugar a desbordamiento de memoria dinámica («heap»).</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 5.24.1-3+deb9u5.</p>

<p>Le recomendamos que actualice los paquetes de perl.</p>

<p>Para información detallada sobre el estado de seguridad de perl, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/perl">https://security-tracker.debian.org/tracker/perl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4347.data"
